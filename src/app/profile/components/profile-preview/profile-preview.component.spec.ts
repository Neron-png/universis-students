import {async, ComponentFixture, TestBed, inject, tick, fakeAsync} from '@angular/core/testing';
import {ProfilePreviewComponent} from './profile-preview.component';
import {SharedModule} from '@universis/common';
import {UserService} from '@universis/common';
import {MostModule} from '@themost/angular';
import {HttpClientTestingModule, TestRequest} from '@angular/common/http/testing';
import {ConfigurationService} from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {ProfileService} from '../../services/profile.service';
import {TestingConfigurationService} from '../../../test';
import {LoadingService} from '@universis/common';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {ApiTestingModule, ApiTestingController} from '@universis/common/testing';

describe('ProfilePreviewComponent', () => {
  let loadingSvc = jasmine.createSpyObj( 'LoadingService', ['showLoading', 'hideLoading']);
  let userSvc = jasmine.createSpyObj('UserService', ['getUser']);
  userSvc.getUser.and.returnValue(Promise.resolve({
    name: 'student@example.com',
    groups: [
      {
        name: 'Students'
      }
    ]
  }));
  let profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  profileSvc.getStudent.and.returnValue(Promise.resolve({
    'person': {
      'familyName': 'Last',
      'givenName': 'First',
      'gender': {
        'alternateName': 'male'
      },
      'email': 'student@example.com',
      'homeAddress': 'Home Road, 40',
      'homePostalCode': '00000',
      'homeCity': 'Home City',
      'homePhone': '+30000000000',
      'temporaryAddress': 'Student Home Road, 50',
      'temporaryPostalCode': '000000',
      'temporaryCity': 'Student Home City',
      'temporaryPhone': '+30000000000',
      'temporaryAddressRegion': null,
      'mobilePhone': '+30690000000'
    },
    'studentIdentifier': '100234360',
    'department': {
      'name': 'Department Name'
    },
    'studyProgram': {
      'name': 'Study Programme'
    },
    'studentStatus': {
      'id': 3,
      'alternateName': 'graduated'
    },
    'semester': 11,
    'inscriptionDate': null,
    'inscriptionSemester': {
      'alternateName': '1'
    },
    'inscriptionPeriod': {
      'alternateName': 'winter'
    },
    'inscriptionYear': {
      'name': '2011-2012'
    },
    'inscriptionMode': {
      'name': 'Inscription Mode'
    },
    'user': {
      'name': 'student@example.com'
    },
    'specialty': 'Study Programme Specialty'
  }));
  let mockApi: ApiTestingController;

    beforeEach(async(() => {
      return TestBed.configureTestingModule({
        declarations: [
          ProfilePreviewComponent
        ],
        imports: [
          HttpClientTestingModule,
          TranslateModule.forRoot(),
          MostModule.forRoot({
            base: '/',
            options: {
              useMediaTypeExtensions: false,
              useResponseConversion: true
            }
          }),
          SharedModule,
          RouterTestingModule,
          ApiTestingModule.forRoot()
        ],
        providers: [
          {
            provide: ConfigurationService,
            useClass: TestingConfigurationService
          },
          {
            provide: ProfileService,
            useValue: profileSvc
          },
          {
            provide: UserService,
            useValue: userSvc
          },
          {
            provide: LoadingService,
            useValue: loadingSvc
          }
        ]
      }).compileComponents();
    }));
    beforeEach(() => {
      mockApi = TestBed.get(ApiTestingController);
      mockApi.match({
        url: '/api/students/me/programGroups/',
        method: 'GET'
      }).map((request: TestRequest) => {
        request.flush(Promise.resolve(Promise.resolve(JSON.parse('{"value":[]}'))));
      })
    });
    it('should have empty inscription date', fakeAsync(() => {
        const fixture = TestBed.createComponent(ProfilePreviewComponent);
        const component = fixture.componentInstance;
        let programGroupSpy = spyOn(component, 'getProgramGroups');
        let counselorsSpy = spyOn(component, 'getCounselors');
        programGroupSpy.and.returnValue(Promise.resolve(JSON.parse('{"value":[]}')));
        counselorsSpy.and.returnValue(Promise.resolve(JSON.parse('{"value":[]}')));
        fixture.detectChanges();
        tick();
        fixture.detectChanges();
        expect(component).toBeTruthy();
        expect(component.loading).toBeFalsy();
        const inscriptionDateElement = fixture.debugElement.query(By.css('.progress_bar__first_top_box>.font-lg'));
        expect(inscriptionDateElement).toBeTruthy();
        expect((<HTMLDivElement>inscriptionDateElement.nativeElement).innerText).toBe('');
    }));
});
